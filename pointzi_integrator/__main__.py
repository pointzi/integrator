from ios import integrate_ios


def get_app_key():
    return input(
        "Please enter your `app_key`, login to the dashboard to find your `app_key` https://dashboard.pointzi.com: "
    )


if __name__ == "__main__":
    integrate_ios(get_app_key())
    print("Check complete")
