#!/bin/env python
from os.path import exists
from subprocess import check_output
import re
from pbxproj import XcodeProject

# https://github.com/kronenthaler/mod-pbxproj/wiki/flags#run-script
PODFILE = """
platform :ios,'13.0' # or above
pod "pointzi"
"""


def create_pod_file(podfile):
    res = check_output(["pod", "init"])
    with open(podfile, "w") as podfile_:
        podfile_.write(PODFILE)


def check_install_cocoapod():
    try:
        res = check_output(["pod", "info"])
    except:
        if (
            input("cocoapods not installed, would you like to install? [y/n]")
            == "y"
        ):
            print("running: `sudo gem install cocoapods`")
            check_output(["sudo", "gem", "install", "cocoapods"])


def check_podfile(podfile):
    if not exists(podfile):
        create_pod_file(podfile)
        print("No `%s` found." % podfile)
    platform = None
    for line in open(podfile, "r"):
        platform = re.match(r"^\s*platform :ios,\s*'(.*)'.*$", line)
        if platform:
            platform = float(platform.groups()[0])
            if platform < 11:
                raise Exception(
                    "Unsupported platform: %s, Minimum supported version is 11.0"
                    % platform
                )
            elif 11 <= platform < 13:
                print(
                    "Using Pointzi legacy sdk, Please update platform to 13 to get the latest and greatest SDK."
                )
            else:
                print("Using latest Pointzi SDK.")
            break
    pointzipod = None
    for line in open(podfile, "r"):
        pointzipod = re.match(r"^\s*pod\s*\"pointzi\".*$", line)
    if not platform:
        raise Exception(
            "Unknown platform. Could not find platform from Podfile"
        )
    if not pointzipod:
        if input("Pointzi not in podfile, would you like to add [y/n]:") == "y":
            with open(podfile, "a") as podfile_:
                podfile_.append('pod "pointzi"')
            check_output(["pod", "update"])
        raise Exception('Please add `pod "pointzi"` to the Podfile')
    return True


def run_pod_install_update():
    result = check_output(["pod", "install"])
    result = check_output(["pod", "update"])


def determine_projectname():
    # TODO
    return "wikipedia"


def determine_bridging_header_file():
    "build > "
    "plutil -convert xml1 -o - myproj.xcodeproj/project.pbxproj"


def add_swift_bridge_header():
    importline = '#import "StreetHawkCore_Pointzi.h"'
    bridging_header_file = "%s-Bridging-Header.h" % determine_projectname()
    with open(bridging_header_file) as f1:
        with open("%s.pointzi.proposed" % bridging_header_file, "w") as f2:
            f2.write(importline)
            for line in f1:
                f2.write(line)


def add_app_delegate_swift():
    """class AppDelegate: UIResponder, UIApplicationDelegate"""
    """        Pointzi.sharedInstance().registerInstall(forApp: "MyApp", withDebugMode: false)"""
    r""".*didFinishLaunchingWithOptions launchOptions.*$"""
    # prompt for shcuid integration


def add_app_delegate_objc():
    """
    - (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
    {
        // Replace 'MyApp' with your app_key, registered in the Pointzi Dashboard.
        [POINTZI registerInstallForApp:@"MyApp" withDebugMode:NO];
        // Set 'withDebugMode' to 'YES' to enable debug logging.
        return YES;
    }"""
    addimport = '#import "StreetHawkCore_Pointzi.h"'
    regex = r""".*didFinishLaunchingWithOptions.*$"""
    addsdk = '[POINTZI registerInstallForApp:@"%s" withDebugMode:NO];' % (
        app_key
    )


def replace_base_classes():
    # Find all base classes: search for classes to override UITableViewController
    # provide list and selection for the ones customer wants to override

    # From	To
    # UITableViewController	StreetHawkBaseTableViewController
    # UIViewController	StreetHawkBaseViewController
    # UICollectionViewController	StreetHawkBaseCollectionViewController
    for srcfile in glob("**/*.c"):
        print(srcfile)


def replace_base_classes_reactnative():
    # - (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
    # {
    #  //Replace UIViewController with StreetHawkBaseViewController
    #  UIViewController *rootViewController = [StreetHawkBaseViewController new];
    # }
    regex = r""".*didFinishLaunchingWithOptions.*$"""
    addsdk = "UIViewController *rootViewController = [StreetHawkBaseViewController new];"


def set_shcuid_objc():
    # //unique id in your system, for example customer's login id
    # Pointzi.sharedInstance().registerInstall(forApp: "MyApp", withDebugMode: false) {
    #    Pointzi.sharedInstance().tagCuid("user@example.com")
    # }
    pass


def set_shcuid_swift():
    # //unique id in your system, for example customer's login id
    # Pointzi.sharedInstance().registerInstall(forApp: "MyApp", withDebugMode: false) {
    #     Pointzi.sharedInstance().tagCuid("user@example.com")
    # }
    pass


def do_build():
    pass


def do_debug_build():
    pass


def integrate_ios(app_key):
    check_install_cocoapod()
    check_podfile("Podfile")
