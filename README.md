# integrator


Tool to integrate Pointzi sdk into apps on supported platforms

Requirements:

- MacOsx
- python2

Setup 

    poetry install

Activate env

    poetry shell

To run:

    python2 pointzi_integrator

Start with running the test cases

    pytest tests/
