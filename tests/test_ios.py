from pointzi_integrator.ios import check_podfile
import pytest

PODFILE_PLATFORM_UNSUPPORTED = """
platform :ios,'10.0' # or above
pod "pointzi"
"""

PODFILE_PLATFORM_LEGACY = """
platform :ios,'11.0' # or above
pod "pointzi"
"""

PODFILE_PLATFORM_LATEST = """
platform :ios,'14.0' # or above
pod "pointzi"
"""


def test_podfile_unsupported_platform(tmp_path):
    d = tmp_path / "ios"
    d.mkdir()
    p = d / "Podfile"
    p.write_text(PODFILE_PLATFORM_UNSUPPORTED)
    assert p.read_text() == PODFILE_PLATFORM_UNSUPPORTED
    assert len(list(tmp_path.iterdir())) == 1
    with pytest.raises(Exception):
        check_podfile(p)


def test_podfile_legacy_platform(tmp_path, capfd):
    d = tmp_path / "ios"
    d.mkdir()
    p = d / "Podfile"
    p.write_text(PODFILE_PLATFORM_LEGACY)
    assert check_podfile(p) is True
    out, err = capfd.readouterr()
    assert "legacy" in out


def test_podfile_latest_platform(tmp_path, capfd):
    d = tmp_path / "ios"
    d.mkdir()
    p = d / "Podfile"
    p.write_text(PODFILE_PLATFORM_LATEST)
    assert check_podfile(p) is True
    out, err = capfd.readouterr()
    assert "latest" in out
